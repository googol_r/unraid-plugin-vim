#!/usr/bin/env bash

set -euo pipefail

main() {
    local -r output_dir="${1}"
    local -r manifests_directory="${2}"
    local -r job_token="${3}"
    local -r package_registry_url="${4}"

    for file_path in ${output_dir}/*; do
        file_name="$(basename "${file_path}")"
        file_url="${package_registry_url}/${file_name}"

        curl --header "JOB-TOKEN: ${job_token}" --upload-file "${file_path}" "${file_url}"
        jq --null-input --arg name "${file_name}" --arg url "${file_url}" --arg filepath "/${file_name}" --compact-output '{ name: $name, filepath: $filepath, url: $url, link_type: "other" }' > "${manifests_directory}/${file_name}.json"

    done
}

main "$@"
