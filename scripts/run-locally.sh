#!/usr/bin/env bash

set -euo pipefail

SCRIPT_DIR="$(dirname "${0}")"
BASE_DIR="$(realpath "${SCRIPT_DIR}/..")"
PACKAGE_NAME=vim
VERSION=development

rm -rf "${BASE_DIR}/working_dir"
mkdir -p "${BASE_DIR}/working_dir/output" "${BASE_DIR}/working_dir/build_root"

cp -r ${BASE_DIR}/package_source/* "${BASE_DIR}/working_dir/build_root/"

docker run --user "$(id --user):$(id --group)" --rm -it -v "${BASE_DIR}/working_dir:/working_dir" -v "${SCRIPT_DIR}:/scripts" aclemons/slackware:15.0 /scripts/create-package.sh "/working_dir/build_root" "/working_dir/output/unraid_plugin_${PACKAGE_NAME}-${VERSION}.tgz"

"${SCRIPT_DIR}/update-plugin-file.sh" "${BASE_DIR}/unraid_plugin.plg" "${BASE_DIR}/working_dir/unraid_plugin_${PACKAGE_NAME}-${VERSION}.tgz" "${VERSION}" "${BASE_DIR}/working_dir/output" "${PACKAGE_NAME}"
