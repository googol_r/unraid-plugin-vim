#!/usr/bin/env bash

set -xeuo pipefail

main() {
    local -r tag_name="${1}"
    local -r manifests_dir="${2}"

    local asset_links=()

    for file in ${manifests_dir}/*; do
        asset_links+=("--assets-link")
        asset_links+=("$(cat "${file}")")
    done

    release-cli create --name "${tag_name}" --tag-name "${tag_name}" "${asset_links[@]}"
}

main "$@"
