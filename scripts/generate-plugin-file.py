#!/usr/bin/env python3

from tempfile import NamedTemporaryFile
import hashlib
import os
import os.path as path
import requests
import sys
import xml.etree.ElementTree as ET

def main():
    plugin_name = sys.argv[1]
    package_path = sys.argv[2]
    version = sys.argv[3]
    output_path = sys.argv[4]

    author = "googol"

    plugin_path = f"/boot/config/plugins/{plugin_name}"

    releases_url = f"https://gitlab.com/googol_r/unraid-plugin-{plugin_name}/-/releases"
    plugin_url = f"{releases_url}/permalink/latest/downloads/unraid-plugin-{plugin_name}.plg"

    slackware_packages = [
        {
            "name": "libsodium",
            "version": "1.0.19",
            "revision": "1",
            "location": "l",
        },
        {
            "name": "vim",
            "version": "9.0.2185",
            "revision": "1",
            "location": "ap",
        },
    ]

    packages = [generate_slackware_package_info(p) for p in slackware_packages] + [generate_package_info(plugin_name, package_path, version, releases_url)]

    plugin = ET.Element("PLUGIN", {
        "name": plugin_name,
        "version": version,
        "author": author,
        "pluginURL": plugin_url,
    })

    for package in packages:
        package_element = ET.SubElement(plugin, "FILE", {
            "Name": os.path.join(plugin_path, package['file_name']),
            "Run": "upgradepkg --install-new"
        })
        url_element = ET.SubElement(package_element, "URL")
        url_element.text = package['url']
        md5_element = ET.SubElement(package_element, "MD5")
        md5_element.text = package['md5']

    remove_script = ""
    for package in reversed(packages):
        remove_script += f"removepkg \"{package['name']}\"\n"
    remove_script += f"rm -rf \"{plugin_path}\"\n"

    remove_script_element = ET.SubElement(plugin, "FILE", {
        "Run": "/bin/bash",
        "Method": "remove"
    })

    remove_script_element.text = remove_script

    ET.indent(plugin)
    tree = ET.ElementTree(plugin)
    with open(path.join(output_path, f"unraid-plugin-{plugin_name}.plg"), "wb") as output_file:
        tree.write(output_file, encoding='utf-8', xml_declaration=True)

def generate_package_info(plugin_name, package_path, version, releases_url):
    package_name = f"unraid_plugin_{plugin_name}-{version}"
    package_file_name = f"{package_name}.txz"
    package_md5 = md5sum_file(package_path)

    return {
        "name": package_name,
        "file_name": package_file_name,
        "url": f"{releases_url}/{version}/downloads/{package_file_name}",
        "md5": package_md5,
    }

def generate_slackware_package_info(slackware_package):
    package_name = f"{slackware_package['name']}-{slackware_package['version']}-x86_64-{slackware_package['revision']}"
    package_file_name = f"{package_name}.txz"
    package_url = f"https://mirrors.slackware.com/slackware/slackware64-current/slackware64/{slackware_package['location']}/{package_file_name}"
    package_md5 = md5sum_url(package_url)

    return {
        "name": package_name,
        "file_name": package_file_name,
        "url": package_url,
        "md5": package_md5,
    }

def md5sum_url(url):
    digest = hashlib.md5()
    download = requests.get(url, stream=True)
    for chunk in download.iter_content(chunk_size=512):
        digest.update(chunk)
    return digest.hexdigest()

def md5sum_file(path):
    digest = hashlib.md5()
    with open(path, "rb") as file:
        for chunk in iterate_file(file, 512):
            digest.update(chunk)

    return digest.hexdigest()

def iterate_file(file, chunk_size):
    while True:
        chunk = file.read(chunk_size)
        if len(chunk) > 0:
            yield chunk
        else:
            break

main()
