#!/usr/bin/env bash

set -euo pipefail

BUILD_ROOT="${1}"
OUTPUT_PATH="${2}"

cd "${BUILD_ROOT}"
makepkg --linkadd y --chown y "${OUTPUT_PATH}"
